import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class testAnnotation {

    public static void main(String[] args) {
        testAnnotation test = new testAnnotation();
        HashMap<String, Object> mapa = new HashMap<String, Object>();
        mapa.put("name", "User");
        mapa.put("age", 30);
        mapa.put("inn", "123456");
        mapa.put("active",true);

        test.createObject(mapa);


    }

    private void createObject(HashMap<String, Object> mapa) {

        Object obj = null;
        try {
            Class myClass = Class.forName("User");
            obj = myClass.newInstance();
            Field[] field = myClass.getDeclaredFields();
            for (Field f : field) {
                Object valueToSetField = mapa.get(f.getName());
                if ((valueToSetField == null) && checkNull(f)) {
                    throw new RuntimeException("Can`t set null into " + f.getName());
                } if (valueToSetField!=null) {
                    SetValueIntoField(f.getName(), valueToSetField, obj);
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        System.out.println(obj.toString());

    }

    private void SetValueIntoField(String name, Object valueField, Object myObj) {
        String methodName = "set"+name.replace(name.charAt(0),name.toUpperCase().charAt(0));
        try {
            Method  method = myObj.getClass().getMethod(methodName, valueField.getClass());
            method.invoke(myObj,valueField);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    private Boolean checkNull(Field field) {
        Nullable nullable = field.getAnnotation(Nullable.class);
        if (nullable != null) {
            if (nullable.value() == true) {
                return false;
            }
        }
        return true;
    }

}
