
public class User {
    @Nullable(false)
    String name;
    @Nullable(true)
    Integer age;
    @Nullable(false)
    String   inn;
    @Nullable(false)
    Boolean active;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    @Override
    public String toString(){
        return "name: "+name+"; Age: "+age+"; inn: "+inn+"; active: "+active;
    }
}
