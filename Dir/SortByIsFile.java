package Dir;

import java.io.File;
import java.util.Comparator;

public class SortByIsFile implements Comparator<File> {
    @Override
    public int compare(File o1, File o2) {
        int a = 0;
        int b = 0;
        if (o1.isFile()) {
            a = 1;
        }
        if (o2.isFile()) {
            b = 1;
        }
        return a - b;
    }
}
