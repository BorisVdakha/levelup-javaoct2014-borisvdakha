package Dir;

import java.io.*;
import java.util.*;

public class PrintDirContents {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        PrintDirContents printDirContents = new PrintDirContents();
        ArrayList<File> arrayList = printDirContents.dirContents(input.nextLine());
        SortByIsFile mySort = new SortByIsFile();
        Collections.sort(arrayList, mySort);
        for (File file : arrayList) {
            if (file.isDirectory()) {
                System.out.println(" <dir>  "+file.getName() );
            } else {
                System.out.println(" <file> "+file.getName());
            }
        }
    }

    public ArrayList<File> dirContents(String pathSource) {
        File sourceDir = new File(pathSource);
        if (sourceDir.exists()) {
            File[] fs = sourceDir.listFiles();
            ArrayList<File> listFiles = new ArrayList<File>();
            if (fs != null) {
                Collections.addAll(listFiles, fs);
                return listFiles;
            }
        }
        return null;
    }
}


