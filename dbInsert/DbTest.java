import java.sql.*;

public class DbTest {
    public static void main(String[] args) {
        DbTest db = new DbTest();
        db.getConnection();
        db.insert();
        db.getConnection();
    }

    public void insert() {
        Connection connection = null;
        PreparedStatement pStatement;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3307/levelup", "root", "usbw");
            pStatement = connection.prepareStatement("INSERT INTO student(FirstName,LastName,Age) VALUES (?,?,?)");
            pStatement.setInt(3, 38);
            pStatement.setString(1, "Roman");
            pStatement.setString(2, "But");
            pStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public void getConnection() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3307/levelup", "root", "usbw");
            statement = connection.createStatement();
            statement.execute("SELECT * FROM student");
            resultSet = statement.executeQuery("SELECT * FROM student");
            while (resultSet.next()) {
                Integer id = resultSet.getInt(4);
                String str = resultSet.getString(2);
                System.out.println( str+ " " +id );
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
