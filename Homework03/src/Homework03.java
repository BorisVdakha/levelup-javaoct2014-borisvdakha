/**
 * Created by Admin on 12.10.14.
 */
public class Homework03 {


    static String[] createArray(int number) {

        String[] array = new String[number];
        for (int i = 0; i < number; i++) {
            array[i] = "" + i + "";
        }
        return array;
    }

    static boolean either24(int... array) {
        boolean firstResult = false;
        boolean secondResult = false;
        int size = array.length;
        for (int i = 0; i < size - 1; i++) {
            if (array[i] == 2) {
                if (array[i + 1] == 2) {
                    firstResult = true;
                }
            }
        }
        for (int i = 0; i < size - 1; i++) {
            if (array[i] == 4) {
                if (array[i + 1] == 4) {
                    secondResult = true;
                }
            }
        }
        return firstResult ^ secondResult;
    }

    static boolean has12(int... array) {
        boolean result = false;
        int size = array.length;
        for (int i = 0; i < size; i++) {
            if (array[i] == 1) {
                for (int j = i; j < size - 1; j++) {
                    if (array[j + 1] == 2) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    static boolean twoTwo(int... array) {
        boolean result = false;
        int size = array.length;
        for (int i = 0; i < size - 1; i++) {
            if (array[i] == 7) {
                if (array[i + 1] == 7) {
                    result = true;
                }
            } else {
                result = false;
            }
        }
        return result;
    }

    static int[] createArray2(int start, int end) {
        int count = end - start;
        int[] array = new int[count];
        for (int i = 0, j = start; j < end; i++, j++) {
            array[i] = j;
        }
        return array;
    }

    static int[] pre4(int... array) {
        int size = array.length;
        int count = 0;
        for (int i = 0; i < size; i++) {
            if (array[i] == 4) {
                count = i;
                break;
            }
        }
        int[] resultArray = new int[count];
        for (int j = 0; j < resultArray.length; j++) {
            resultArray[j] = array[j];
        }
        return resultArray;
    }

    static int[] zeroFront(int... array) {
        int size = array.length;
        int count = 0;
        int tmp;
        for (int i = 0; i < size; i++) {
            if (array[i] == 0) {
                tmp = array[count];
                array[count] = array[i];
                array[i] = tmp;
                count++;
            }
        }
        return array;
    }

    static int[] evenOdd(int... array) {
        int size = array.length;
        int count = 0;
        int tmp;
        for (int i = 0; i < size; i++) {
            if (array[i] % 2 == 0) {
                tmp = array[count];
                array[count] = array[i];
                array[i] = tmp;
                count++;
            }
        }
        return array;
    }


    static int sum67(int... array) {
        int size = array.length;
        int sum = 0;
        int i = 0;
        int j = 0;
        for (i = 0; i < size; i++) {
            if (array[i] == 6) {
                do {
                    ++i;
                } while (array[i-1] != 7);
                if (i>=size)break;
            }
            sum += array[i];
        }
        return sum;
    }

    static boolean tripleUp(int... array) {
        boolean result = false;
        int size = array.length;
        for (int i = 0; i < size - 2; i++) {
            if (array[i] == array[i + 1] - 1 && array[i] == array[i + 2] - 2) {
                result = true;
            }
        }
        return result;
    }

    public static void main(String[] args) {

        System.out.println(twoTwo(4, 7, 7, 3));
        System.out.println(twoTwo(7, 7, 4, 7));

        int[] mas = createArray2(5, 10);
        for (int i : mas) {
            System.out.print(i + ", ");
        }
        System.out.println();

        System.out.println(has12(1, 3, 2));
        System.out.println(has12(3, 1, 4, 5, 6));

        System.out.println(either24(1, 2, 2));
        System.out.println(either24(4, 4, 1, 2, 2));

        String[] str = createArray(4);
        for (String i : str) {
            System.out.print(i + ", ");
        }
        System.out.println();

        int[] mas2 = pre4(1, 2, 4, 1);
        for (int i : mas2) {
            System.out.print(i + ", ");
        }
        System.out.println();

        int[] mas3 = zeroFront(1, 0, 0, 1);
        for (int i : mas3) {
            System.out.print(i + ", ");
        }
        System.out.println();

        int[] mas4 = evenOdd(1, 0, 1, 0, 0, 1, 1);
        for (int i : mas4) {
            System.out.print(i + ", ");
        }
        System.out.println();

        System.out.println(sum67(1, 2, 2));
        System.out.println(sum67(1, 2, 2, 6, 99, 99, 7));
        System.out.println(sum67(1, 1, 6, 7, 2));

        System.out.println(tripleUp(1, 4, 5, 6, 2));
        System.out.println(tripleUp(1, 2, 4));
    }


}
