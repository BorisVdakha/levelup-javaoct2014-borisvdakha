/**
 * Created by Admin on 26.10.14.
 */
public class Test {

    public static boolean makeBricks(int firstNum, int secondNum, int thirdNum) {
        return ((firstNum + secondNum * 5) >= thirdNum) && (thirdNum % 5 <= firstNum);
    }

    public static int fixTeen(int num) {
        if ((num >= 13 && num < 15) || (num > 16 && num <= 19)) {
            return 0;
        } else {
            return num;
        }
    }

    public static int noTeenSum(int firstNum, int secondNum, int thirdNum) {
        return fixTeen(firstNum) + fixTeen(secondNum) + fixTeen(thirdNum);
    }

    public static int blackjack(int firstNum, int secondNum) {
        if ((firstNum <= 0) || (secondNum <= 0)) {
            System.out.println("Numbers must be bigger than zero");
            {
                return 0;
            }
        } else if ((firstNum > 21) && (secondNum > 21)) {
            {
                return 0;
            }
        } else if ((firstNum <= 21) && (secondNum <= 21)) {
            if (firstNum > secondNum) {
                return firstNum;
            } else {
                return secondNum;
            }
        } else {
            if (firstNum > secondNum) {
                return secondNum;
            } else {
                return firstNum;
            }
        }
    }

    public static void main(String[] args) {

        System.out.println(makeBricks(3, 1, 8));
        System.out.println(makeBricks(3, 1, 9));
        System.out.println(makeBricks(0, 3, 11));
        System.out.println(makeBricks(1, 3, 11));
        System.out.println(makeBricks(5, 2, 14) + "\n");

        System.out.println(noTeenSum(1, 2, 3));
        System.out.println(noTeenSum(2, 13, 1));
        System.out.println(noTeenSum(2, 1, 14));
        System.out.println(noTeenSum(1, 15, 19) + "\n");

        System.out.println(blackjack(19, 21));
        System.out.println(blackjack(19, 22));
        //System.out.println(17%5);


    }
}
