
public class TempClass {
    private int intField;
    private boolean booleanField;

    public TempClass() {
    }

    public TempClass(boolean booleanField, int intField) {
        this.booleanField = booleanField;
        this.intField = intField;
    }

    public int getIntField() {
        return intField;
    }

    public void setIntField(int intField) {
        this.intField = intField;
    }

    public boolean isBooleanField() {
        return booleanField;
    }

    public void setBooleanField(boolean booleanField) {
        this.booleanField = booleanField;
    }
}
