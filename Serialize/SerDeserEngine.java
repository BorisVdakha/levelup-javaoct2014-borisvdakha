import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.Scanner;

public class SerDeserEngine {

    public static String PATH_SOURCE = "D:\\1";
    public static String NAME_OF_FILE = "Serialize.txt";

    public void SerializeObject(Object object) {
        Class serObj = object.getClass();
        try {
            PrintWriter myPrint = new PrintWriter(createFile(PATH_SOURCE, NAME_OF_FILE).getAbsoluteFile());
            myPrint.println(serObj.getCanonicalName());
            Field[] fields = serObj.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                myPrint.println(field.getName() + ":" + field.getType() + ":" + field.get(object));
            }
            myPrint.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public Object DeSerializeObject(File file) {
        if (file.exists()) {
            String className = null;
            String fieldType = null;
            String fieldName = null;
            String fieldValue = null;
            String str = null;
            int numberFirst = 0;
            int numberSecond = 0;
            try {
                Scanner scan = new Scanner(file);
                if (scan.hasNext()) {
                    className = scan.nextLine();
                    Class serializeClass = Class.forName(className);
                    Object myObj = serializeClass.newInstance();
                    while (scan.hasNext()) {
                        str = scan.nextLine();
                        numberFirst = str.indexOf(':');
                        numberSecond = str.lastIndexOf(':');
                        fieldName = str.substring(0, numberFirst);
                        fieldType = str.substring(numberFirst + 1, numberSecond);
                        fieldValue = str.substring(numberSecond + 1);
                        Field field = serializeClass.getDeclaredField(fieldName);
                        field.setAccessible(true);
                        if (fieldType.equals("int")) {
                            field.setInt(myObj, Integer.parseInt(fieldValue));
                        } else if (fieldType.equals("float")) {
                            field.setFloat(myObj, Float.parseFloat(fieldValue));
                        } else if (fieldType.equals("double")) {
                            field.setDouble(myObj, Double.parseDouble(fieldValue));
                        } else if (fieldType.equals("char")) {
                            field.set(myObj, (fieldValue));
                        } else if (fieldType.equals("byte")) {
                            field.setByte(myObj, Byte.parseByte(fieldValue));
                        } else if (fieldType.equals("long")) {
                            field.setLong(myObj, Long.parseLong(fieldValue));
                        } else if (fieldType.equals("boolean")) {
                            field.setBoolean(myObj, Boolean.parseBoolean(fieldValue));
                        } else field.set(myObj, null);
                    }
                    return myObj;
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public File createFile(String pathSource, String nameOfFile) {
        File dir = new File(pathSource);
        dir.mkdirs();
        File f = new File(dir, nameOfFile);
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }


}
