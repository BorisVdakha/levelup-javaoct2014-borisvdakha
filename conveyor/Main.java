import java.lang.Object;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        ArrayList<Part> bodyArrayList = new ArrayList<Part>();
        for (int i = 0; i < 300; i++) {
            bodyArrayList.add(new Body(String.valueOf(i)));
        }
        ArrayList<Part> engineArrayList = new ArrayList<Part>();
        for (int i = 0; i < 300; i++) {
            engineArrayList.add(new Engine(String.valueOf(i)));
        }
        ArrayList<Part> transmissionArrayList = new ArrayList<Part>();
        for (int i = 0; i < 300; i++) {
            transmissionArrayList.add(new Transmission(String.valueOf(i)));
        }

        HashMap<String, ArrayList<Part>> mapDetails = new HashMap<String, ArrayList<Part>>();
        mapDetails.put("body", bodyArrayList);
        mapDetails.put("engine", engineArrayList);
        mapDetails.put("transmission", transmissionArrayList);

        for (int i = 0, j=5; i < 5; i++) {
            Bmw[] bmw = new Bmw[j];
            bmw[i]=new Bmw();
            main.createObject(bmw[i], mapDetails);
            try{
            System.out.println(bmw[i]);}
            catch (NullPointerException e){
                System.out.println("Закончились детали! Автомобиль № "+(i+1)+" не собран");
            }
        }
        for (int i = 0, j=5; i < 5; i++) {
            Opel[] opel = new Opel[j];
            opel[i]=new Opel();
            main.createObject(opel[i], mapDetails);
            try{
                System.out.println(opel[i]);}
            catch (NullPointerException e){
                System.out.println("Закончились детали! Автомобиль № "+(i+1)+" не собран");
            }
        }
    }

    private void createObject(Car car, HashMap<String, ArrayList<Part>> mapDetails) {
        Class myClass = car.getClass();

        Field[] field = myClass.getDeclaredFields();
        for (Field f : field) {
            ArrayList list = mapDetails.get(f.getName());
            for (int i = 0; i < list.size(); i++) {

                if (!checkApplication(f, list.get(i))) {
                    continue;
                }
                SetValueIntoField(f.getName(), list.get(i), car);
                list.remove(i);
                break;
            }
        }
    }

    private static Boolean checkApplication(Field field, Object part) {
        Applicable applicable = field.getAnnotation(Applicable.class);
        Class partClass = part.getClass();
        try {
            Field fieldCode = partClass.getField("code");
            String value = (String) fieldCode.get(part);
            for (int i = 0; i < applicable.parts().length; i++) {
                if (applicable.parts()[i].equals(value)) {
                    return true;
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void SetValueIntoField(String name, Object valueField, Object myObj) {
        String methodName = "set" + name.toUpperCase().charAt(0) + name.substring(1);
        try {
            Method method = myObj.getClass().getMethod(methodName, valueField.getClass());
            method.invoke(myObj, valueField);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
