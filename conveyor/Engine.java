
public class Engine extends Part {

   public String code;

    public Engine() {
    }

    public Engine(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
