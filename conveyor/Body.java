
public class Body extends Part{
   public String code;

    public Body() {
    }

    public Body(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
