import java.util.Iterator;

public class HolderCollection implements Iterable {
    public Holder head = null;
    public int size = 0;

    public void add(Integer data) {
        if (size == 0) {
            head = new Holder(data);
        } else {
            Holder temp = head;
            for (int i = 0; i < size - 1; i++) {
                temp = temp.next;
            }
            temp.next = new Holder(data);
        }
        size++;
    }

    public void add(Integer data, int index) {
        if (index <= size && index >= 0) {
            if (index == 0) {
                Holder hold = new Holder(data);
                hold.next = head;
                head = hold;
            } else {
                Holder temp = head;
                for (int i = 0; i < index - 1; i++) {
                    temp = temp.next;
                }
                Holder hold = new Holder(data);
                hold.next = temp.next;
                temp.next = hold;
            }
            size++;
        }
    }

    public void remove() {
        if (size > 0) {

            Holder temp = head;
            for (int i = 0; i < size - 2; i++) {
                temp = temp.next;
            }
            temp.next = null;
        }
        size--;
    }

    public void remove(int index) {
        if (index <= size - 1 && index >= 0) {
            if (index == 0) {
                head = head.next;
            } else {
                Holder temp = head;
                for (int i = 0; i < index - 1; i++) {
                    temp = temp.next;
                }
                temp.next = temp.next.next;
            }
            size--;
        }

    }


    public Integer get(int index) {
        Holder temp = head;
        for (int i = 0; i < size; i++) {
            if (index == i) {
                return temp.data;
            } else {
                temp = temp.next;
            }
        }
        return null;
    }

    public Integer get() {
        return head.data;
    }

    @Override
    public Iterator iterator() {
        return new HolderCollectionIterator();
    }

    private class HolderCollectionIterator implements Iterator {
        int currentPosition;

        @Override
        public boolean hasNext() {
            return currentPosition < size;
        }

        @Override
        public Integer next() {
            return HolderCollection.this.get(currentPosition++);
        }

        @Override
        public void remove() {

        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
