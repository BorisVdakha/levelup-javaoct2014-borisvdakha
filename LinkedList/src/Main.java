import java.util.Iterator;
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        HolderCollection holderCollection = new HolderCollection();
        holderCollection.add(1);
        holderCollection.add(2);
        holderCollection.add(3);
        holderCollection.add(4);
        holderCollection.add(77, 0);
        holderCollection.add(88, 2);
        holderCollection.add(99, 4);
        holderCollection.add(5);
        holderCollection.add(6);
        holderCollection.add(7);

        System.out.println("holderCollection.size: " + holderCollection.size);
        holderCollection.remove(6);
        for (int i = 0; i < 10; i++) {
            System.out.println(holderCollection.get(i));
        }
//        for (Object i : holderCollection) {
//            System.out.println(i);
//        }
//        Iterator iter = holderCollection.iterator();
//        while (iter.hasNext()){
//            System.out.println(iter.next());
//        }
        // System.out.println(holderCollection.get(3));

    }

}
