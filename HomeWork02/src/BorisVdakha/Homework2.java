package BorisVdakha;

/**
 * Created by Admin on 10.10.14.
 */
public class Homework2 {

    static String wrapWord(String firstString, String secondString) {
        return firstString.substring(0, 2) + secondString + firstString.substring(2, 4);
    }

    static String firstHalf(String firstString) {
        int half = firstString.length() / 2;
        return firstString.substring(0, half);
    }

    static String nonStart(String firstString, String secondString) {
        return firstString.substring(1) + secondString.substring(1);
    }

    static boolean hasBad(String firstString) {
        if (firstString.length() < 3) {
            return false;
        } else {
            return firstString.substring(0, 3).equals("bad");
        }
    }

    static String withoutX(String firstString) {
        int size = firstString.length();

        if (firstString.startsWith("x")) {
            firstString = firstString.substring(1);
            size = firstString.length();
        }
        if (firstString.endsWith("x"))
            firstString = firstString.substring(0, size - 1);

        return firstString;
    }

    static String makeTags(String firstString, String secondString) {
        String tagBegin = "<" + firstString + ">";
        String tagEnd = "</" + firstString + ">";
        return tagBegin + secondString + tagEnd;
    }

    static String atFirst(String firstString) {
        int size = firstString.length();
        if (size == 1) {
            return firstString.substring(0) + "@";
        } else if (size == 0) {
            return "@" + "@";
        } else {
            return firstString.substring(0, 2);
        }

    }

    static String comboString(String firstString, String secondString) {
        int sizeFirstString = firstString.length();
        int sizeSecondString = secondString.length();
        if (sizeFirstString > sizeSecondString)
            return secondString + firstString + secondString;
        else
            return firstString + secondString + firstString;
    }

    static String right2(String firstString) {
        int size = firstString.length();
        return firstString.substring(size - 2, size) + firstString.substring(0, size - 2);
    }

    static String minCat(String firstString, String secondString) {
        int sizeFirstString = firstString.length();
        int sizeSecondString = secondString.length();
        int size;
        if (sizeFirstString > sizeSecondString) {
            size = sizeFirstString - sizeSecondString;
            firstString = firstString.substring(size, sizeFirstString);
            return firstString + secondString;
        } else if (sizeFirstString < sizeSecondString) {
            size = sizeSecondString - sizeFirstString;
            secondString = secondString.substring(size, sizeSecondString);
            return firstString + secondString;
        } else return firstString + secondString;

    }

    public static void main(String[] args) {

        System.out.println(wrapWord("{{}}", "word"));

        System.out.println(firstHalf("HelloThere"));

        System.out.println(nonStart("Hello", "There"));

        System.out.println(hasBad("badxx"));
        System.out.println(hasBad("xxbadxx"));

        System.out.println(withoutX("xHix"));
        System.out.println(withoutX("xHi"));
        System.out.println(withoutX("Hxix"));
        System.out.println(withoutX("x"));

        System.out.println(makeTags("i", "Yay"));
        System.out.println(makeTags("i", "Hello"));
        System.out.println(makeTags("cite", "Yay"));

        System.out.println(atFirst("hello"));
        System.out.println(atFirst("hi"));
        System.out.println(atFirst("h"));
        System.out.println(atFirst(""));

        System.out.println(comboString("Hello", "hi"));

        System.out.println(right2("Hello"));
        System.out.println(right2("java"));
        System.out.println(right2("Hi"));

        System.out.println(minCat("Hello", "Hi"));
        System.out.println(minCat("Hello", "java"));
        System.out.println(minCat("java", "Hello"));


    }
}
