import java.io.*;

public class SelectImage implements FilenameFilter {

    @Override
    public boolean accept(File dir, String name) {
        File f = new File(String.valueOf(dir)+"\\"+name);
        if (f.isFile()) {
            if (name.contains(".")) {
                name = name.substring(0, name.lastIndexOf("."));
                return name.toLowerCase().contains("image");
            } else {
                return name.toLowerCase().contains("image");
            }
        } else {
            return true;
        }
    }
}
