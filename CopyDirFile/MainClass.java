import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class MainClass {
    public static void main(String[] args) {
        copyDirFile("c:\\1", "c:\\2", true);
    }

    public static void copy(File source, File dest) throws IOException {
        Files.copy(source.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    public static void createDir(String path) {
        File f = new File(path);
        f.mkdirs();
    }

    public static void copyDirFile(String pathSource, String pathDest, boolean catalogStructure) {
        File sourceDir = new File(pathSource);
        File destDir = new File(pathDest);
        if (sourceDir.exists()) {
            FilenameFilter select = new SelectImage();
            File[] fs = sourceDir.listFiles(select);
            try {
                for (File value : fs) {
                    File fileDest = new File(destDir.getPath() + "//" + value.getName());
                    if (value.isFile()) {
                        createDir(pathDest);
                        copy(value, fileDest);
                    } else {
                        if (catalogStructure) {
                            copyDirFile(String.valueOf(value), String.valueOf(fileDest), true);
                        } else {
                            copyDirFile(String.valueOf(value), pathDest, false);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
