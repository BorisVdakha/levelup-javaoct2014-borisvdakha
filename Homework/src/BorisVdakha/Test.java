package BorisVdakha;

import BorisVdakha.com.AirTransport;
import BorisVdakha.com.LandTransport;

/**
 * Created by boris on 08.10.14.
 */
public class Test {
    public static void main(String[] args) {

        LandTransport car = new LandTransport("Машина", 4, 140, "Белый", 8.9, 4, 2, 410.5);
        System.out.println(car.toString());
        car.needFuel(234);
        car.addPassenger(2);
        System.out.println("Количество пассажиров: " + car.getPassengerNumber() + " человек");
        car.deletePassenger(1);
        System.out.println("Количество пассажиров: " + car.getPassengerNumber() + " человек");
        car.start();
        car.stop();
        System.out.println();

        AirTransport copter = new AirTransport();
        copter.setName("Вертолет");
        copter.setMaxPassengersNumber(6);
        copter.setMaxSpeed(600);
        copter.setColor("Зеленый");
        copter.setFuelConsumption(35);
        copter.setCrewNumber(2);
        copter.setMaxFlyingHeight(4000);
        System.out.println(copter.toString());
        copter.start();
        copter.flyUp(2400);
        copter.flyUp(750);
        copter.flyDown(70);
        System.out.println("Полет проходит на высоте " + copter.getFlyingHeight() + " м. Приятного полета.");
        copter.stop();


    }
}
