package BorisVdakha.com;

/**
 * Created by boris on 08.10.14.
 */
abstract public class Transport {
    protected String name;
    protected int maxPassengersNumber;
    protected double maxSpeed;
    protected String color;
    protected double fuelConsumption;

    public Transport() {

    }

    public Transport(String name, int maxPassengersNumber, double maxSpeed, String color, double fuelConsumption) {
        this.name = name;
        this.maxPassengersNumber = maxPassengersNumber;
        this.maxSpeed = maxSpeed;
        this.color = color;
        this.fuelConsumption = fuelConsumption;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setMaxPassengersNumber(int MaxPassengersNumber) {
        this.maxPassengersNumber = MaxPassengersNumber;
    }

    public int getMaxPassengersNumber() {
        return maxPassengersNumber;
    }

    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getSpeed() {
        return maxSpeed;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public String toString() {
        return "Название транспортного средства: " + name +
                "\nВместимость пассажиров: " + maxPassengersNumber + " человек" + "\nМаксимальная скорость: " +
                maxSpeed + " км/ч" + "\nЦвет: " + color +
                "\nРасход топлива на 100 км: " + fuelConsumption + " литров";
    }

    public void needFuel(int distance) {
        if (fuelConsumption == 0)
            System.out.println("Установите значение расхода топлива");
        else
            System.out.println("Чтобы преодолеть " + distance + " км " + "необходимо затратить " +
                    (fuelConsumption / 100 * distance) + " литров топлива");

    }
    public abstract void start();
    public abstract void stop();
}
