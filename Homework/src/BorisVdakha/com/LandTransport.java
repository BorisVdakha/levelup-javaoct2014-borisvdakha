package BorisVdakha.com;

/**
 * Created by boris on 08.10.14.
 */
public class LandTransport extends Transport {
    protected int wheelNumber;
    protected int passengerNumber;
    protected double freeTrunkVolume;


    LandTransport() {

    }

    public LandTransport(String name, int maxPassengerNumber, double maxSpeed, String color,
                         double fuelConsumption, int wheelNumber, int passengerNumber, double freeTrunkVolume) {
        super(name, maxPassengerNumber, maxSpeed, color, fuelConsumption);
        this.wheelNumber = wheelNumber;
        this.passengerNumber = passengerNumber;
        this.freeTrunkVolume = freeTrunkVolume;

    }

    public void setWheelNumber(int wheelNumber) {
        this.wheelNumber = wheelNumber;
    }

    public int getWheelNumber() {
        return wheelNumber;
    }

    public void setPassengerNumber(int passengerNumber) {
        this.passengerNumber = passengerNumber;
    }

    public int getPassengerNumber() {
        return passengerNumber;
    }

    public void setFreeTrunkVolume(float freeTrunkVolume) {
        this.freeTrunkVolume = freeTrunkVolume;
    }

    public double getFreeTrunkVolume() {
        return freeTrunkVolume;
    }

    public String toString() {
        return super.toString() + "\nКоличество колес: " + wheelNumber + " шт" +
                "\nКоличество пассажиров: " + passengerNumber + " человек" + "\nОбъем багажника: " +
                freeTrunkVolume + " литров";
    }


    public void addPassenger(int passengerNumber) {
        this.passengerNumber += passengerNumber;
        if (this.passengerNumber > maxPassengersNumber) {
            System.out.println("Нельзя добавить такое количество людей. Максимальное количество пассажиров:" + maxPassengersNumber);
            this.passengerNumber -= passengerNumber;
        }
    }

    public void deletePassenger(int passengerNumber) {
        this.passengerNumber -= passengerNumber;
        if (this.passengerNumber < 0) {
            System.out.println(name + " не имеет такого количества пассажиров");
            this.passengerNumber -= passengerNumber;
        }

    }

    public void start() {
        System.out.println(name + " готовится ехать...");
    }

    public void stop() {
        System.out.println("Движение прекращено ");
    }
}

