package BorisVdakha.com;

/**
 * Created by boris on 08.10.14.
 */
public class AirTransport extends Transport {

    protected int crewNumber;
    protected double maxFlyingHeight;
    protected double flyingHeight;

    public AirTransport() {

    }

    public AirTransport(String name, int passengersNumber, double maxSpeed, String color,
                        double fuelConsumption, int crewNumber, double maxFlyingHeight) {
        super(name, passengersNumber, maxSpeed, color, fuelConsumption);
        this.crewNumber = crewNumber;
        this.maxFlyingHeight = maxFlyingHeight;
    }

    @Override
    public void setMaxPassengersNumber(int MaxPassengersNumber) {
        if (MaxPassengersNumber > 1000) {
            this.maxPassengersNumber = 1000;
        } else
            this.maxPassengersNumber = MaxPassengersNumber;
    }

    public int getCrewNumber() {
        return crewNumber;
    }

    public void setCrewNumber(int crewNumber) {
        this.crewNumber = crewNumber;
    }

    public double getMaxFlyingHeight() {
        return maxFlyingHeight;
    }

    public void setMaxFlyingHeight(double maxFlyingHeight) {
        this.maxFlyingHeight = maxFlyingHeight;
    }

    public double getFlyingHeight() {
        return flyingHeight;
    }

    public String toString() {
        return super.toString() + "\nКоличество членов экипжа: " + crewNumber + " человек" +
                "\nМаксимальная высота полета: " + maxFlyingHeight + " м";
    }


    public void flyDown(double flyingHeight) {
        this.flyingHeight -= flyingHeight;
        if (this.flyingHeight < 0) {
            this.flyingHeight = 0;
            System.out.println(name + " приземлился");
        } else
            System.out.println(name + " опустился на " + flyingHeight + " м");

    }

    public void flyUp(double flyingHeight) {
        this.flyingHeight += flyingHeight;
        if (maxFlyingHeight == 0)
            System.out.println("Неустановлена максимальная высота полета");
        else if (this.flyingHeight > maxFlyingHeight)
            System.out.println(name + " не может взлететь на такую высоту");
        else
            System.out.println(name + " поднялся на " + flyingHeight + " м");

    }

    public void flyUp() {
        System.out.println("Атас, это OVERLOAD");
    }

    public void start() {
        System.out.println(name + " готовится к взлету...");
    }

    public void stop() {
        System.out.println("Полет прекращен");
    }
}

