package printUniqFileNames;

import java.io.*;
import java.util.*;

public class PrintFileNames {
    static Set<String> uniqFileNames = new TreeSet<String>();

    public static void main(String[] args) {
        PrintFileNames printFileNames = new PrintFileNames();
        printFileNames.uniqFile("c:\\1");
        for (String name : uniqFileNames) {
            System.out.println(name);
        }
    }

    public void uniqFile(String pathSource) {
        File sourceDir = new File(pathSource);
        if (sourceDir.exists()) {
            File[] fs = sourceDir.listFiles();
            if (fs != null) {
                for (File file : fs) {
                    if (!file.isFile()) {
                        uniqFile(file.getPath());
                    } else {
                        uniqFileNames.add(file.getName());
                    }
                }
            }
        }
    }
}