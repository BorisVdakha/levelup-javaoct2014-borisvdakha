/**
 * Created by boris on 22.10.14.
 */
public class RussianGreeting implements Greeting {
    private String name;

    RussianGreeting() {
        name = "Пользователь";
    }

    RussianGreeting(String name) {
        this.name = name;
    }

    @Override
    public void greet() {
        greetWithName(this.name);
    }

    @Override
    public void greetWithName(String name) {
        System.out.println("Привет " + name);
    }
}
