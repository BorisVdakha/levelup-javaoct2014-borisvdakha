/**
 * Created by boris on 22.10.14.
 */
public class Test {
    static void printGreeting(Greeting greeting) {
        greeting.greet();
    }


    public static void main(String[] args) {
        Greeting englishGreating = new EnglishGreeting("Leonardo");
        printGreeting(englishGreating);
        Greeting russianGreating = new RussianGreeting();
        printGreeting(russianGreating);
        Greeting ukraineGreating = new Greeting() {

            @Override
            public void greet() {
                greetWithName("Користувач");
            }

            @Override
            public void greetWithName(String name) {
                System.out.println("Привіт " + name);
            }
        };
        printGreeting(ukraineGreating);
    }
}
