/**
 * Created by boris on 22.10.14.
 */
public class EnglishGreeting implements Greeting{
    private String name;
    EnglishGreeting() {
        name = "User";
    }

    EnglishGreeting(String name) {
        this.name = name;
    }

    @Override
    public void greet() {
        greetWithName(this.name);
    }

    @Override
    public void greetWithName(String name) {
        System.out.println("Hello " + name);
    }
}

