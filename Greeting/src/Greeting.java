/**
 * Created by boris on 22.10.14.
 */
public interface Greeting {
    void greet();
    void greetWithName(String name);


}
