/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 13.11.14
 * Time: 20:23
 * To change this template use File | Settings | File Templates.
 */

import java.io.*;

public class Main {
    public static void main(String[] args) {
        File javaFile = new File("c:/1/test.txt");
        File res = new File("c:/1/result.txt");
        if (javaFile.exists()) {
            try (BufferedReader f = new BufferedReader(new FileReader(javaFile))) {
                BufferedWriter f2 = new BufferedWriter(new FileWriter(res));
                String b;
                while ((b = f.readLine()) != null) {
                    if (b.contains("java")) {
                        f2.write(b);
                        f2.newLine();
                    }
                }
                f2.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
