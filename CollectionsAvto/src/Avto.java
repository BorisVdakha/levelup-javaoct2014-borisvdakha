/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 06.11.14
 * Time: 19:06
 * To change this template use File | Settings | File Templates.
 */
public class Avto {
    private String name;
    private String color;
    private String number;

    public Avto() {
    }

    public Avto(String name, String color, String number) {
        this.name = name;
        this.color = color;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return " Color: " + color + ";" + " Number: " + number + ";" + " Name: " + name + ";";
    }

}
