import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 06.11.14
 * Time: 19:09
 * To change this template use File | Settings | File Templates.
 */
public class Test {

    public static void main(String[] args) {
        ArrayList<Avto> arrayAvto = new ArrayList<Avto>();
        arrayAvto.add(new Avto("Zaz", "White", "01"));
        arrayAvto.add(new Avto("Mersedes", "Black", "02"));
        arrayAvto.add(new Avto("Lada", "Red", "03"));
        arrayAvto.add(new Avto("BMW", "Blue", "04"));
        arrayAvto.add(new Avto("Opel", "Red", "05"));
        arrayAvto.add(new Avto("Opel", "Blue", "03"));

        Collections.sort(arrayAvto, new Comparator<Avto>() {
            @Override
            public int compare(Avto avto, Avto avto2) {
                int len1 = avto.getColor().length();
                int len2 = avto2.getColor().length();
                int n = Math.min(len1, len2);
                char v1[] = avto.getColor().toCharArray();
                char v2[] = avto2.getColor().toCharArray();
                int k = 0;
                while (k < n) {
                    char c1 = v1[k];
                    char c2 = v2[k];
                    if (c1 != c2) {
                        return c1 - c2;
                    }
                    k++;
                }
                return len1 - len2;

            }
        });

        for (int i = 0; i < arrayAvto.size(); i++) {
            System.out.println(arrayAvto.get(i).getColor());
        }

        Set<Avto> uniqSetByNumber = new TreeSet<Avto>(new Comparator<Avto>() {
            @Override
            public int compare(Avto avto, Avto avto2) {
                if (avto.getNumber().equals(avto2.getNumber())) {
                    return 0;
                }
                return 1;
            }
        });
        uniqSetByNumber.addAll(arrayAvto);
        System.out.println(uniqSetByNumber.toString());

        Set<Avto> uniqSetByColor = new TreeSet<Avto>(new Comparator<Avto>() {
            @Override
            public int compare(Avto avto, Avto avto2) {
                if (avto.getColor().equals(avto2.getColor())) {
                    return 0;
                }
                return 1;
            }
        });
        uniqSetByColor.addAll(arrayAvto);
        System.out.println(uniqSetByColor.toString());

        Collections.sort(arrayAvto, new Comparator<Avto>() {
            @Override
            public int compare(Avto avto, Avto avto2) {
                int len1 = avto.getName().length();
                int len2 = avto2.getName().length();
                int n = Math.min(len1, len2);
                char v1[] = avto.getName().toCharArray();
                char v2[] = avto2.getName().toCharArray();
                int k = 0;
                while (k < n) {
                    char c1 = v1[k];
                    char c2 = v2[k];
                    if (c1 != c2) {
                        return c1 - c2;
                    }
                    k++;
                }
                return len1 - len2;

            }
        });

        Set<Avto> uniqSetByName = new TreeSet<Avto>(new Comparator<Avto>() {
            @Override
            public int compare(Avto avto, Avto avto2) {
                if (avto.getName().equals(avto2.getName())) {
                    return 0;
                }
                return 1;
            }
        });
        uniqSetByName.addAll(arrayAvto);
        System.out.println(uniqSetByName.toString());



    }


}
