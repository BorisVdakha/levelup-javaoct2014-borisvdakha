import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test {
    public static void main(String[] args) {
        try {
            Class bookClass = Class.forName("Book");
            Object objBook = bookClass.newInstance();
            Field fieldAuthor= bookClass.getDeclaredField("author");
            fieldAuthor.setAccessible(true);
            fieldAuthor.set(objBook,"Herbert Wells");
            Field fieldName= objBook.getClass().getDeclaredField("name");
            fieldName.setAccessible(true);
            fieldName.set(objBook,"The Invisible Man");
            Class readerClass = Class.forName("Reader");
            Object objReader = readerClass.newInstance();
            Field fieldFirstName= objReader.getClass().getDeclaredField("firstName");
            fieldFirstName.setAccessible(true);
            fieldFirstName.set(objReader,"Boris");
            Field fieldlastName= objReader.getClass().getDeclaredField("lastName");
            fieldlastName.setAccessible(true);
            fieldlastName.set(objReader,"Vdakha");
            Field fieldAge= objReader.getClass().getDeclaredField("age");
            fieldAge.setAccessible(true);
            fieldAge.setInt(objReader,30);
            Field fieldBook= objReader.getClass().getDeclaredField("book");
            fieldBook.setAccessible(true);
            fieldBook.set(objReader,objBook);
            Method methodCreateBook = readerClass.getMethod("createBook",String.class,String.class);
            methodCreateBook.invoke(objReader,"Jules Verne","Voyage au centre de la Terre");
            Method methodReadBook = readerClass.getMethod("readBook");
            methodReadBook.invoke(objReader);




        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (NoSuchFieldException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }


    }


}
