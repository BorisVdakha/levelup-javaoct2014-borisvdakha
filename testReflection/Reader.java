
public class Reader {
    private String firstName;
    private String lastName;
    private int age;
    private Book book;

    public Reader() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public void createBook(String author, String name ){
        Book book = new Book (author, name);
        this.book = book;
    }
    public void readBook(){
        System.out.println(book.getAuthor()+" \""+book.getName()+"\""+" is reading");
    }
}
